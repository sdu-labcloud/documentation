# Overview

The admin documentation contains the following chapters:

- [Node setup](node-setup.md)
- [Smart node](smart-node.md)
