# Contribution guidelines

This section covers guidelines for the contribution to the platform.

## Services

This section of the documentation covers standards for the usage of third-party services and documents the services being used.

### Naming conventions

For the setup of third party services there are some naming conventions, that should be adhered:

- The regular name is:  
  `SDU - The Core`
- If a description can be provided, it should be provided with the value:  
  `Maintainers of the manufacturing platform "The Core".`
- If a name for the usage within URLs is required, the following [slug](https://en.wikipedia.org/wiki/Clean_URL#Slug) should be used:  
  `sdu-thecore`

### Services

To get access to any of the services, write a mail to **nicklas.frahm@gmail.com** and you should receive an invitation within a short amount of time. Please make sure, you send the mail from the email that you plan to use within the systems.

#### Slack

The Slack workspace used is located at [https://sdu-thecore.slack.com](https://sdu-thecore.slack.com/).

#### Google Drive

There is a Google Drive folder for miscellaneous files, that are not tracked with Git.

#### GitLab

To manage source code and perform project management, GitLab is used. The group is [**SDU - The Core**](https://gitlab.com/sdu-thecore).

## Tools

This section of the documentation covers the installation and setup of the recommended development tools.

!> The installation guides were created on Windows 10. However, the installation may be similar on other operating systems.

### Git

!> Git is **required** in order to work with the source code, so its installation is considered to be **mandatory**.

Start out by downloading the Git installer from the [official website](https://git-scm.com). Then, start the installer and follow the instructions in this document.

![Git license consent](../images/tools-git-license.png)

Click `Next >`.

![Git component selection](../images/tools-git-components.png)

Select the components as shown in the image above and click `Next >`. Even though the Git GUI may be checked by default, it is strongly recommended to use the Git command line instead of the GUI. The main reason for this is that the command line is much more versatile and once the concepts of Git are understood, it can be adapted by the user without a lot of effort.

![Git editor selection](../images/tools-git-editor.png)

As you'll be mainly working from the command line it is recommend to choose **Nano** here as it is a relatively easy to learn text editor for the command line. After you selected your editor, click `Next >`.

![Git command line integration](../images/tools-git-command-line.png)

Make sure to select **Use Git from the Windows Command Prompt** to use Git conveniently, either from the normal Windows command prompt, the Powershell or also the Git Bash, that will be installed alongside Git. Then, click `Next >`.

![Git SSH client](../images/tools-git-ssh.png)

Select **Use OpenSSH**. This is also the SSH client you will find preinstalled on most Linux distributions. It can serve a variety of purposes, including the connection to the command line of remote servers, which is one of the most common use cases. Click `Next >`.

![Git SSL library](../images/tools-git-ssl-library.png)

Keep the default **Use the OpenSSL library** and click `Next >`.

![Git line ending conversions](../images/tools-git-line-endings.png)

Make sure to select **Checkout as-is, commit Unix-style line endings** and click `Next >`.

![Git terminal emulator](../images/tools-git-terminal-emulator.png)

Select **Use MinTTY (the default terminal of MSYS2)** and click `Next >`.

![Additional Git features](../images/tools-git-additional-features.png)

Be sure to tick all checkboxes and finally click `Install`. Wait for the installation to finish and eventually click `Finish`.

### VS Code

VS Code is a very lightweight and extendable text editor. Head to the [VS Code website](https://code.visualstudio.com/), download the installer for your operating system and install it.

#### Extensions

Once you have VS Code installed, head to the extensions tab.

![VS Code extensions](../images/tools-vscode-extensions.png)

Search and install the following extensions:

- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) by **Esben Petersen**
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) by **EditorConfig**

If you used the [Atom editor](https://atom.io) before, you might also want to install the [One Dark Pro](https://marketplace.visualstudio.com/items?itemName=zhuangtongfa.Material-theme) theme by **zhuangtongfa** via the extensions tab.

Once you have the extensions installed, press `CTRL + ,`. This will open the `settings.json`, which contains the settings of your VS Code instance. Edit the file until it looks similar to the file shown below.

```json
{
  "files.eol": "\n",
  "editor.tabSize": 2,
  "editor.formatOnSave": true,
  "workbench.colorTheme": "One Dark Pro Vivid"
}
```

The directive `"files.eol": "\n"` configures VS Code to use linux style line endings, which are generally easiear to work with across different platforms. `"editor.tabSize": 2` configures existing tabs to be rendered as two spaces or, if configured, to insert two spaces, when the tab key is pressed. `"editor.formatOnSave": true` configures _Prettier_ to format your files automatically, when you save them. You can omit the `"workbench.colorTheme": "One Dark Pro Vivid"`, if you did not install the aforementioned theme extension.

## Git

This section provides usage examples and best practices, when working with Git.

### Description

The best description for Git can be found on its [official website](https://git-scm.com):

> Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. Git is easy to learn and has a tiny footprint with lightning fast performance. It outclasses SCM tools like Subversion, CVS, Perforce, and ClearCase with features like cheap local branching, convenient staging areas, and multiple workflows.

### Everyday usage

In this part we will cover the basic commands for the everyday work with the Git command line. Let's assume that you just start your day. The first order of business is to get the most recent changes from the server. We assume you are on the default branch, which you contribute to, and that you already have a local copy of the remote repository on your computer. In this example we will use **master** for reference. So, to get the most recent changes, simply run:

```shell
$ git pull
```

Now you are up-to-date. You are working on a web project and you have been assigned the task to implement a password reset form, consisting of a mail input field and a button. Start out by creating a `feature`-branch:

```shell
$ git checkout -b feature/reset-password-form
```

> If you should create a branch along the day, you would always checkout on **master** first and do `git pull` before creating the new branch. This reduces the chance of getting merge conflicts.

Right after creating your branch, make sure to push it to the server:

```shell
$ git push -u origin feature/reset-password-form
```

After some time, you implemented a reusable button component. This would be a good point to commit. Begin by staging the files that you want to commit.

```shell
$ git add <filename>
```

In some cases it might be desired to add all changes. In such cases you can do `git add .` to add all changes. Be careful if you do so and use `git status` to ensure that you did not commit files, you did not mean to commit. If you want to unstage files, just run:

```shell
$ git reset <filename>
```

After you staged all necessary files, you can get an overview with:

```shell
$ git status
```

You verified, that everything you need is there. Now you can create a commit. You can do it by running:

```shell
$ git commit -m "Add reusable button component (#42)"
```

> Git commit messages should always be a small description of what this commit changes. Also, they should be formulated in the **imperative**. Tasks can be linked by using a `#` (number sign) and the task ID.

After some more commits, you are done. You can push all local commits to your remote branch:

```shell
$ git push
```

Before you create a merge request, make sure to checkout on your default branch again:

```shell
$ git checkout master
```

### Tagging

Git tags mark important or special points in history like new features, bugfixes or breaking changes. When such a change is implemented, the version should be bumped according to the [semantic versioning specification 2.0.0](https://semver.org/). This can be done by help with Git by simply executing a few commands after a commit that has introduced such changes. The commands, shown below, will create an annotated tag.

```shell
$ git tag -a 0.2.0
```

To do this, Git will open an editor to supply a message. Once the editor opens, enter a message in the form of a small changelog:

```txt
For changes, see the CHANGELOG.md.

#
# Write a message for tag:
#   0.2.0
# Lines starting with '#' will be ignored.
```

You may abort the process by quitting the editor without saving.

By saving the editor, you finish creating your tag. Afterwards you can list all existing tags by running:

```shell
$ git tag
```

To review a tag more in detail, you can type:

```shell
$ git show 0.2.0
```

If you want to remove the tag again and you haven't pushed it yet, you can do that by typing:

```shell
$ git tag -d 0.2.0
```

If everything is correct, you can push the tag to the git server.

```shell
$ git push --tags
```

## GitLab

This part of the documentation covers the workflow in GitLab and the usage of it as management and development tool.

### Groups

Groups represent a team contributing to the development of a piece of software. This is not limited to developers, but also includes mangers and stakeholders, which are able to populate the backlog with issues and organize the issues.

### Milestones

Milestones will be used to represent sprints within GitLab, when applying the [Scrum](https://en.wikipedia.org/wiki/Scrum_%28software_development%29) methodology. They should be created on a group level and not per repository. A milestone should be set up to have a defined start date and a defined end date. The time period of the milestones should be constant, therefore not allowing any modification of the start and the end date afterwards. Remaining work has to be moved to the next sprint. Milestones should have a descriptive name that compiles the main goal into a small sentence. The description should not be populated.

![GitLab milestone](../images/gitlab-milestone.png)

### Labels

Labels are used to give additional visibility and filtering capabilities to the issues. Similar to the milestones, they should be created on a group level and not per repository. There are several groups of recommended labels. Every issue should be only assigned one label of each label group, meaning that an issue may not have two `kind/*` labels, but it may have multiple labels from distinct groups, like `kind/feature`, `area/development` and `status/approved`.

- `kind/*` labels are supposed to indicate what an issue is about without giving any technical background. Therefore the following labels should be created:
  - `kind/feature`
  - `kind/bug`
- `area/*` labels indicate the work area these issues are about. The are supposed to signalize, which competencies are required to perform a given task. The current recommendation is:
  - `area/development`
  - `area/management`
  - `area/deployment`
  - `area/documentation`
  - `area/testing`
- `status/*` labels give additional information about the state of an issue. The states will be explained further in the [issue section](#issues). The recommendation here is:

  - `status/new`
  - `status/approved`
  - `status/committed`
  - `status/done`
  - `status/removed`

![GitLab labels](../images/gitlab-labels.png)

### Issues

GitLab offers the possibility to create issues, which can be used to track bugs, new features and other work.

#### Scope

An issue should be a small task that can typically be accomplished by a single person in up to a single day of work, which would be equivalent to eight hours of work. Breaking work down into small items is not always straightforward, but is required to prevent opaque and stalling work, which does not benefit the final product. However, issues should also not be scoped to micromanage the developers, instead they should leave room for an optimized implementation by the specialized developer. Additionally issues should be formulated to allow anybody with the according technical background to execute them. Therefore, they should provide the context of the task well enough and should include a compact but thorough description of the work to be done.

#### Lifecycle

An issue can either be open or closed. Issues have a certain lifecycle to verify their relevance and to schedule them according to their priority. If a new issue is created, an `area/*` label and the `kind/*` label should be assigned. Every issue that describes new requirements and therefore new functionality should be assigned the `kind/feature` label. Every issue that documents missing or unintended behaviour should be assigned the `kind/bug` label. A milestone and a due date should not be assigned as this information will be automatically added, when the issue is moved to the sprint backlog. See [sprint execution](#sprint-execution) for further information. Additionally if a developer creates an issue, he should add the `status/new` label. The issue is then reviewed by the product owner and assigned the label `status/approved` or `status/removed`, depending on whether it is relevant or not. If a stakeholder or product owner creates an new issue, he can directly assign the `status/approved` label to it as the user need is verified through the owner of the issue. Once work on an issue is started, it should be assigned the label `status/committed`. If the work on an issue is done, it may be assigned the label `status/done`. If the work is determined not to be relevant anymore, e.g. due to a change of requirements, the issue should be assigned the label `status/removed`. At no point, issues should be deleted as they might be used for further reference later on. Issues should not be closed by a developer but rather by the product owner in the beginning of the sprint planning. Only issues with the labels `status/done` and `status/removed` may be closed.

![GitLab issue](../images/gitlab-issue.png)

### Sprint execution

In the sprint planning the developers should move issues to the sprint backlog by assigning the current milestone to the according issues. The developers will then assign themselves to issues along the sprint. If a developer finished all his work for a given sprint and there is time left, he should request additional work from his product owner. In contrast if work is not finished, the issue should be reevaluated in the sprint planning. If it is still relevant, it should be moved to the next sprint by assigning the according milestone.
