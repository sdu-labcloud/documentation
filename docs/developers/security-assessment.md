# Security assessment

This sections documents the security of the project and outlines possible attack vectors.

## Node token spoofing

It is possible to obtain the MAC address and the serial number of the simple nodes and use these to initiate a new session against the server. The server will not allow any modification until an administrator grants access to a session token.

### Possible attack vectors

- Denial of service (DoS) caused by invalidating the currently active session
- Denial of service (DoS) caused by cutting power supply

