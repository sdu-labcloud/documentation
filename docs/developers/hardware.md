# Hardware

## Simple Node

### Bill of Material

|      Component       | Price (DKK)     | Source                                                                                                   |
| :------------------: | --------------- | -------------------------------------------------------------------------------------------------------- |
|     Sonoff Basic     | 79.00           | [ArduinoTech](https://arduinotech.dk/shop/sonoff-basic-wifi-wireless-smart-switch-mqtt-coap-smart-home/) |
| Eccel Chilli-UART-B1 | 182.70          | [RS Online](https://dk.rs-online.com/web/p/rfid-moduler/1526487/)                                        |
|       WS2812B        | 3.00            | [ArduinoTech](https://arduinotech.dk/shop/ws2812b-led-chips-with-heatsink-10mmx3mm-5050-smd-rgb-dc5v/)   |
|   Cable Gland (2)    | 9.10            | [RS Online](https://dk.rs-online.com/web/p/kabelforskruninger/8829941/)                                  |
|  Case (3D printed)   | 5.00 (estimate) |                                                                                                          |
|      Total Cost      | 278.80          |                                                                                                          |

## Strong node

### Bill of Material

|      Component       | Price (DKK)     | Source                                                                                                   |
| :------------------: | --------------- | -------------------------------------------------------------------------------------------------------- |
|     Sonoff Basic     | 79.00           | [ArduinoTech](https://arduinotech.dk/shop/sonoff-basic-wifi-wireless-smart-switch-mqtt-coap-smart-home/) |
| Eccel Chilli-UART-B1 | 182.70          | [RS Online](https://dk.rs-online.com/web/p/rfid-moduler/1526487/)                                        |
|       WS2812B        | 3.00            | [ArduinoTech](https://arduinotech.dk/shop/ws2812b-led-chips-with-heatsink-10mmx3mm-5050-smd-rgb-dc5v/)   |
|   Cable Gland (2)    | 9.10            | [RS Online](https://dk.rs-online.com/web/p/kabelforskruninger/8829941/)                                  |
|   4 Pole Contactor   | 133.39          | [RS Online](https://dk.rs-online.com/web/p/kontaktorer/6851318/)                                         |
|  Case (3D printed)   | 5.00 (estimate) |                                                                                                          |
|      Total Cost      | 412.19          |                                                                                                          |

## Smart Node

### Bill of Material

|       Component       | Price (DKK)     | Source                                                                                                 |
| :-------------------: | --------------- | ------------------------------------------------------------------------------------------------------ |
|   Raspberry Pi 3B+    | 219.10          | [RS Online](https://dk.rs-online.com/web/p/processor-og-mikrocontroller-udviklingssaet/1373331)        |
| Eccel Chilli-UART-B1  | 182.70          | [RS Online](https://dk.rs-online.com/web/p/rfid-moduler/1526487/)                                      |
|        WS2812B        | 3.00            | [ArduinoTech](https://arduinotech.dk/shop/ws2812b-led-chips-with-heatsink-10mmx3mm-5050-smd-rgb-dc5v/) |
|    Cable Gland (2)    | 9.10            | [RS Online](https://dk.rs-online.com/web/p/kabelforskruninger/8829941/)                                |
|    5V Relay Board     | 23.75           | [DK Volt](https://www.dkvolt.dk/produkt/relae-dc/)                                                     |
| HDR-15-5 Power Supply | 80.25           | [Cypax](https://www.cypax.dk/vare/HDR-15-5)                                                            |
|  32 GB MicroSD Card   | 54.09           | [RS Online](https://dk.rs-online.com/web/p/sd-kort/1805818/)                                           |
|   Case (3D printed)   | 5.00 (estimate) |                                                                                                        |
|      Total Cost       | 576.99          |                                                                                                        |

## Cases

The STEP and STL files for the cases can be found [here](https://drive.google.com/open?id=1SVwGFyn6ZRwZOV-7v3Vul3c5MBANw5cM)
