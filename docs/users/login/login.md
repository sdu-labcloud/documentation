# Logging in

To log in, navigate to [https://thecore.sdu.dk](https://thecore.sdu.dk) and click on one of the two `Log in`-Buttons.

![Dashboard before login](./login-00.png)

Afterwards, type your university email and the password to your university account.

![University login page](./login-01.png)

You are then redirected to the dashboard. The menu shows more options now.

![Dashboard after login](./login-02.png)
