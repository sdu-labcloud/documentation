# Contribution

This file describes how you can contribute to the project.

## Content

This project uses **docsify** to generate and serve the documentation. To add further documentation create a new `.md`-file in the `docs/markdown` folder of this repository, add all required images to the `docs/images` folder and add an entry to the `docs/_sidebar.md` file that looks similar to this:

```markdown
<!-- docs/_sidebar.md -->

- [menu-headline](filename.md)
```

For further information refer to [docsify's reference documentation](https://docsify.js.org).

## Branching

We use feature branches in this project. So before you create any commits for an issue, that has the label `kind/feature`, check out on a new branch via:

```shell
$ git checkout -b feature/add-security documentation
```

If you develop against and issue that has the label `kind/bug`, create a bug branch:

```shell
$ git checkout -b feature/add-security-documentation
```

Once you created commits, push your branch to the remote **origin** and create a new merge request:

```shell
$ git push -u origin feature/add-security-documentation
```
